package com.example.demo;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


public final class App {

    /**
     * Parses telemetry data
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        // Using Scanner for Getting Input from User
        System.out.println("Enter in the full telemetry ingest data");
        try (Scanner in = new Scanner(System.in)) {
            String path = in.nextLine();

            Path fp = Paths.get(path);
            SatelliteTelemetryParser stp = new SatelliteTelemetryParser();
            stp.readFile(fp);
        }
    }
}
