package com.example.demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;


public class SatelliteTelemetryParser {
    
    public Map<String,List<Date>> lowReadingDates = new HashMap<>();
    public Map<String,List<Date>> highReadingDates = new HashMap<>();
    public Map<String,Integer> lowReadings = new HashMap<>();
    public Map<String,Integer> highReadings = new HashMap<>();
    public List<Map<String,Object>> records = new ArrayList<>();
    public List<String> existingLowRecords = new ArrayList<>(); 
    public List<String> existingHighRecords = new ArrayList<>();
    
    /**
     * Parses log file line by line and prints JSON results
     * @param filepath - Location of the telemetry log file
     * @throws IOException
     */
    public void readFile(Path filepath) throws IOException  {
        Files.lines(filepath)
        .forEach( ln -> {
            try {
                this.recordEntry(ln);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        JSONArray ja = new JSONArray();
        this.records.forEach(m -> {
            ja.put(m);
        });
        System.out.println(ja.toString(4));
    }
    
    /**
     * Parses a line delimited by `|` and keeps track of occurence count and dates for low
     * and high events. Only reports 1 event per log file.
     * @param line
     * @throws ParseException
     */
    public void recordEntry(String line) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.ENGLISH);
        String severity = "";
        String[] telemetryRow = line.split("\\|");
        Map<String, Object> record = new HashMap<>();
        
        Float value = Float.parseFloat(telemetryRow[6]);
        Integer redLowLimit = Integer.parseInt(telemetryRow[5]);
        Integer redHighLimit = Integer.parseInt(telemetryRow[2]);
        Boolean insertValue = false;
        
        if (value < redLowLimit) {
            insertValue = true;
            severity = "Red Low";
        } else if (value > redHighLimit) {
            insertValue = true;
            severity = "Red High";
        }

        // If value exceeds threshold record the event
        if (insertValue) {
            record.put("timestamp", telemetryRow[0]);
            record.put("satelliteId", Integer.parseInt(telemetryRow[1]));
            record.put("component", telemetryRow[7]);
            String satelliteId = String.valueOf(record.get("satelliteId"));
            if (severity.equals("Red Low")) {
                Integer count = this.lowReadings.get(satelliteId);
                if (count == null) {
                    this.lowReadings.put(satelliteId, 1);
                } else {
                    this.lowReadings.put(satelliteId, count + 1);
                }             
                List<Date> dates = this.lowReadingDates.get(satelliteId);
                if (dates == null) {
                    dates = new ArrayList<>();
                }
                dates.add(formatter.parse(telemetryRow[0]));
                this.lowReadingDates.put(satelliteId, dates);
                // If the number of recorded events is more then 3 check if it is within time limit threshold
                if (dates.size() >= 3 && !this.existingLowRecords.contains(satelliteId)) {
                    long lastDate = dates.get(dates.size() - 1).getTime();
                    final String sev = severity;
                    dates.forEach(date -> {
                        if (lastDate != date.getTime()) {
                            Long diff = lastDate - date.getTime();
                            Long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);  
                            Boolean isWithinRange = minutes < 5L;
                            if (isWithinRange && !this.existingLowRecords.contains(satelliteId)) {
                                record.put("timestamp", formatter.format(date));
                                record.put("severity", sev);
                                this.existingLowRecords.add(satelliteId);
                                this.records.add(record);
                            }
                        }
                    });
                }
            // If the number of recorded events is more then 3 check if it is within time limit threshold
            } else if (severity.equals("Red High")) {
                Integer count = this.highReadings.get(satelliteId);
                if (count == null) {
                    this.highReadings.put(satelliteId, 1);
                } else {
                    this.highReadings.put(satelliteId, count + 1);
                }
                List<Date> dates = this.highReadingDates.get(satelliteId);
                if (dates == null) {
                    dates = new ArrayList<>();
                }
                dates.add(formatter.parse(telemetryRow[0]));
                this.highReadingDates.put(satelliteId, dates);
                if (dates.size() >= 3 && !this.existingHighRecords.contains(satelliteId)) {
                    long lastDate = dates.get(dates.size() - 1).getTime();
                    final String sev = severity;
                    dates.forEach(date -> {
                        if (lastDate != date.getTime()) {
                            Long diff = lastDate - date.getTime();
                            Long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);  
                            Boolean isWithinRange = minutes < 5L;
                            if (isWithinRange && !this.existingHighRecords.contains(satelliteId)) {
                                record.put("timestamp", formatter.format(date));
                                record.put("severity", sev);
                                this.existingHighRecords.add(satelliteId);
                                this.records.add(record);
                            }
                        }
                    });
                }
            }
        }
    }
}
